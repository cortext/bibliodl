# bibliodl

bibliodl is a Python library and a command line tool to collect and download
bibliometric data from online resourses, such as OpenAlex, Crossref, WoS, among
others

bibliodl stands for "Bibliometric Downloader"

## Automated workflow

Impact and difusion metrics collected from the APIs below:
- PlumX https://plu.mx
- Altmetric https://www.altmetric.com
- Crossref https://www.crossref.org

Search document title from the APIs:
- OpenAlex https://openalex.org
- Crossref https://www.crossref.org

Data formats used for inputs, outputs and datasets:
- BibTeX
- CSV
- JSON

Possible API source to collect more metrics:
- https://www.dimensions.ai

Service to ask where is PDF available for some publication:
- https://unpaywall.org

Persistent identifiers for research organizations:
- https://ror.org

## Python dependencies

- python3-bibtexparser
- python3-httpx
- python3-crossrefapi
- [habanero](https://pypi.org/project/habanero)
- [pyaltmetric](https://pypi.org/project/pyaltmetric)
- [pyalex](https://pypi.org/project/pyalex)
- [opencitingpy](https://pypi.org/project/opencitingpy)
- [thefuzz](https://pypi.org/project/thefuzz)

## Folders

- `bin/` scripts, one script per project, and examples
- `cache/` filesystem cache for the external api responses, used mainly to avoid redo many of the same request during development phase
- `input/` folder with input files used by scripts (not in git repo)
- `log/` log files
- `output/` folder with output files created by the scripts (not in git repo)
- `raw/` (planned) folder to save raw data, such as JSON returned by the external APIs
- `src/` the bibliodl modules and the methods to collect, retrieve and parse data, wos, openalex, crossref, etc

## Quickstart

install python dependencies
```sh
poetry install
```

set the environment variables
```sh
UA_EMAIL=***
WOS_APIKEY=***
IEEEXPLORE_APIKEY=***
PLUMX_APIKEY=***
```

collect cortext citation metadata and metrics:
```sh
poetry run python3 bin/cortext
```

collect authors metadata:
```sh
poetry run python3 bin/author
```

collect pesti-sci WoS data:
```sh
poetry run python3 bin/pesti-sci
```

### Fulltext query

create the `.env` file:
```sh
IEEEXPLORE_APIKEY=**********
IEEEXPLORE_SEARCHSTRING='("Full Text .AND. Metadata":analizo)'
OPENALEX_SEARCHSTRING='(analizo AND ("static analysis" OR "parser" OR "analise estatica" OR software OR tool))'
```

collect citations metadata by fulltext query:
```sh
poetry run python3 bin/search
```

## Run pesti-sci script with logs

```sh
poetry run python3 bin/pesti-sci > pesti-sci-$(date +%Y%m%d).log
```

or simply:
```sh
make run-pesti-sci
```

## Build

if you have poetry installed just run
```sh
poetry build
```

otherwise install python3 `build` module and run
```sh
python3 -m build .
```

## References

- [What is Fuzzy Matching?](https://redis.com/blog/what-is-fuzzy-matching)

## Author

Joenio Marques da Costa <joenio@joenio.me>

## License

This project is licensed under the GNU General Public License v3.0 or later -
see the [COPYING](/COPYING) file for details
