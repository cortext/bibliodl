# bibliodl roadmap

## pesti-sci backlog:

- [x] double check empty response for some WOS old year, such as WOS:A1974U972200001
  - [x] run with empty cache on unicorn and check results
- [x] colllect adress, organization, city, country etc for each author, separate by `***`
- [x] add column WOS `coll_id` to CSV
- [ ] rename wos.match to wos.search, refactor pesti-sci script
- [x] save raw json collected from wos

## cortext publications backlog:

- [ ] aggregate metrics from multiple sources into one single metric
- [x] search DOI for documents missing it
- [ ] enrich the initial list of CorTexT citations
- [ ] add support for https://www.dimensions.ai
- [x] build a graph of direct and indirect CorTexT citations
- [ ] collect authors and affiliations for each paper and policy doc
  - [ ] create new `bin/author`: cmdline to fetch authors metadata
  - [x] rename `bin/direct` to `bin/citation`: cmdline to collect citation metadata and metrics
  - [ ] use ROR to collect organization data
- [ ] collect forward citation metadata

## other:

- [ ] check concurrent request from https://gitlab.com/cortext/knowmak/rpd-knowmak-tagger
- [ ] refactor code to merge into bibliodbs repository
- [ ] use json-ld format as input and output
