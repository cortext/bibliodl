ifneq ("$(wildcard .env)","")
  include .env  # load VARIAVLES defined on .env file
  export        # this exports all VARIABLES to commands in this Makefile
endif

setup:
	mkdir -p log
	mkdir -p raw/crossref
	mkdir -p raw/openalex
	mkdir -p raw/wos

run-pesti-sci: setup
	unbuffer poetry run python3 bin/pesti-sci | tee --append log/pesti-sci-$(shell date +%Y%m%d).log
