import re
from thefuzz import fuzz

def normalize(title):
  return title.replace(" ", "").replace("-", "").lower()

def nop(title):
  return title

# remove the main title part before the ':'
def subtitle(title):
  return re.sub(r'^[^:]+\s*:\s*', '', title)

# remove subtitle part after the ':'
def maintitle(title):
  return re.sub(r'\s*:\s*[^:]+$', '', title)

# remove all non-ascii characters
def nonascii(title):
  return title.encode("ascii", errors='ignore')

# remove punctuations
def nonpunctuation(title):
  return title.replace(",", "").replace(";", "").replace(".", "")

# remove brackets, parenthesis, etc
def nonbracket(title):
  return title.replace("[", "").replace("]", "").replace(".", "")

# calculate fuzzy distance, string similarity
# fuzz.ratio calculates the normalized Indel distance between 2 strings
# see: https://rapidfuzz.github.io/RapidFuzz/Usage/fuzz.html#ratio
def ratio(a, b):
  minimum = 90
  return fuzz.ratio(a.lower(), b.lower()), minimum

# remove single quote
def nonquote(title):
  return title.replace("'", "").replace("’", "")
