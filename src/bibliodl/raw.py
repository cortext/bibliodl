import json
import os.path

def write(api, data):
  if data:
    filename = f'raw/{api.name}/{api.filename(data)}'
    if not os.path.exists(filename):
      with open(filename, "w") as file:
        print(f'I: [raw] write file "{filename}"')
        file.write(json.dumps(data, indent=2))
        return True
  print(f'I: [raw] no overwrite "{filename}"')
  return False
