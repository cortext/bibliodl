from cachy import CacheManager

_cache = False
path = None
namespace = 'a1'

# search DOI and PMID by TITLE
def search(api, title):
  if not path:
    return api.search(title)
  key = f"{namespace}_search_{title}_{api.__name__}"
  global _cache
  if path and not _cache:
    config = {
      'stores': {
        'file': {
          'driver': 'file',
          'path': path
        }
      }
    }
    _cache = CacheManager(config)
  data = _cache.get(key)
  if not data:
    data = api.search(title)
    if data:
      _cache.put(key, data, 43200) # cache for 30d
    else:
      _cache.put(key, 'NotFound', 7200) # cache for 5d
  elif data == 'NotFound':
    # avoid a new http request for a previous NotFound content
    return False
  return data
