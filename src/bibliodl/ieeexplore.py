import httpx

# IEEEXplore API documentation:
# https://developer.ieee.org/

# IEEEXplore Phytho3 reference code:
# https://developer.ieee.org/Python3_Software_Development_Kit

# IEEEXplore API JSON examples
# https://ieeexploreapi.ieee.org/api/v1/search/articles?apiKey=$KEY

# Get metadata for a single DOI
# https://ieeexploreapi.ieee.org/api/v1/search/articles?apiKey=$KEY&doi=10.1109/CVPR.2016.90

# IEEEXplore WebUI URL search example:
#https://ieeexplore.ieee.org/search/searchresult.jsp?action=search&newsearch=true&matchBoolean=true&queryText='("Full Text .AND. Metadata":analizo)'

apiKey = ""
searchstring = ""

def query(searchstring):
  params = {
    'apiKey': apiKey,
    'querytext': searchstring,
    'max_records': 200, # 200 is the maximum allowed by the ieeexplore api
    'start_record': None, # needed to pagination if the total records found is > then 200
  }
  count = 0
  last_page = False
  articles = []
  while not last_page:
    params['start_record'] = count + 1
    data = httpx.get("https://ieeexploreapi.ieee.org/api/v1/search/articles", params=params)
    if data and data.status_code == httpx.codes.OK:
      data = data.json()
      articles = articles + data['articles']
      print(f"I: [ieeexplore] fulltext search found {data['total_records']} articles")
      count = count + params['max_records']
      if count >= data['total_records']:
        last_page = True
  if articles:
    return articles
  else:
    return False

def find_doi(data):
  if data and 'doi' in data and data['doi']:
    return data['doi'].lower()
  return False

def feed(row, data):
  fed = False
  if 'isbn' in data:
    row['isbn'] = data['isbn']
    fed = True
  if 'issn' in data:
    row['issn'] = data['issn']
    fed = True
  if 'publication_year' not in row and 'publication_year' in data:
    row['publication_year'] = data['publication_year']
    fed = True
  if 'document_title' not in row and 'title' in data:
    row['document_title'] = data['title']
    fed = True
  if fed:
    # add __name__ to api_source column
    name = __name__.split('.')[-1]
    if 'api_source' not in row:
      row['api_source'] = name
    else:
      if row['api_source'].find(name) < 0:
        row['api_source'] = f"{row['api_source']}+{name}"
