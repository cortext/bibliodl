from pyaltmetric import Altmetric, Citation
from pyaltmetric.pyaltmetric import HTTPException

# Altmetric API documentation
# https://api.altmetric.com/data-endpoints-counts.html

# Altmetric API JSON example
# https://api.altmetric.com/v1/doi/10.1177/20552076231169832

altmetric = Altmetric()

def fetch(doi):
  try:
    data = altmetric.doi(doi)
    if data:
      print(f'I: [altmetric] fetch doi "{doi}"')
      return data
    else:
      print(f'W: [altmetric] empty response for doi "{doi}"')
      return False
  except HTTPException as err:
    print(f'E: [altmetric] HTTP error for doi "{doi}" - {err=}')
    return False
  except Exception as err:
    print(f'E: [altmetric] unknown error for doi "{doi}" - {err=}')
    return False

def feed(row, data):
  if not 'altmetric_policy_citations' in row and 'cited_by_policies_count' in data:
    # cited_by_policies_count: Number of policies that have mentioned this publication
    row['altmetric_policy_citations'] = data['cited_by_policies_count']
  if not 'altmetric_patents_citations' in row and 'cited_by_patents_count' in data:
    # cited_by_patents_count: Number of patents that have mentioned this publication
    row['altmetric_patents_citations'] = data['cited_by_patents_count']
  if not 'altmetric_tweeters' in row and 'cited_by_tweeters_count' in data:
    # cited_by_tweeters_count: Number of twitter accounts that have tweeted this publication
    row['altmetric_tweeters'] = data['cited_by_tweeters_count']
  if not 'altmetric_facebook' in row and 'cited_by_fbwalls_count' in data:
    # cited_by_fbwalls_count: Number of walls that have mentioned the output on Facebook
    row['altmetric_facebook'] = data['cited_by_fbwalls_count']
  if not 'altmetric_wikipedia' in row and 'cited_by_wikipedia_count' in data:
    # cited_by_wikipedia_count: Number of wikipedia pages that have cited this publication
    row['altmetric_wikipedia'] = data['cited_by_wikipedia_count']
  if not 'altmetric_score' in row and 'score' in data:
    # score: Current Altmetric Attention Score
    row['altmetric_score'] = data['score']
  if not 'altmetric_readers' in row and 'readers_count' in data:
    # readers_count: Total number of unique users who have saved this article in Mendeley, CiteULike or Connotea
    row['altmetric_readers'] = data['readers_count']
  if not 'altmetric_uniq_citations' in row and 'cited_by_accounts_count' in data:
    # cited_by_accounts_count: The number of unique sources referencing the research output and is the sum of all cited_by_* entries
    row['altmetric_uniq_citations'] = data['cited_by_accounts_count']
  # add __name__ to api_source column
  name = __name__.split('.')[-1]
  if 'api_source' not in row:
    row['api_source'] = name
  else:
    if row['api_source'].find(name) < 0:
      row['api_source'] = f"{row['api_source']}+{name}"
  return False
