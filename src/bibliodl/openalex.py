import pyalex
from pyalex import Works
import re
import time
from bibliodl.util import string as util

# OpenAlex API documentation
# https://docs.openalex.org/api-entities/works/search-works

# OpenAlex API JSON example
# https://api.openalex.org/autocomplete/works?q=A%20constru%C3%A7%C3%A3o%20do%20g%C3%AAnero%20no%20discurso%20conservador:%20uma%20an%C3%A1lise%20de%20coment%C3%A1rios%20em%20rede%20social

# OpenAlex API JSON example with DOI
# https://api.openalex.org/works/https://doi.org/10.1155/2014/413629

# Example of boolean Query full text:
# https://api.openalex.org/works?search=%28elmo%20AND%20%22sesame%20street%22%29%20NOT%20%28cookie%20OR%20monster%29
# (search for works that mention "elmo" and "sesame street," but not the words "cookie" or "monster")

# PyAlex library source code repository
# https://github.com/J535D165/pyalex

pyalex.config.email = ''
searchstring = ""

name = __name__.split('.')[-1]

def fetch(doi):
  doi_url = doi
  if not re.match("^http", doi_url, re.I):
    doi_url = "https://doi.org/" + doi
  try:
    data = Works()[doi_url]
    if data:
      print(f'I: [openalex] fetch doi "{doi}"')
      return data
    else:
      print(f'W: [openalex] empty response for doi "{doi}"')
  except Exception as e:
    print(f"E: [openalex] {e}")
  return False

def search(title):
  title_safe = title.replace(",", "").replace("&", "")
  data = Works().search_filter(title=title_safe).get()
  max_ratio = 0
  if data:
    functions = [
      [util.nop, util.nop],
      [util.subtitle, util.nop],
      [util.maintitle, util.nop],
      [util.nonascii, util.nonascii],
      [util.nonpunctuation, util.nonpunctuation],
      [util.nonbracket, util.nonbracket]
    ]
    for i in data:
      ratio, minimum = util.ratio(i['title'], title)
      if ratio > max_ratio:
        max_ratio = ratio
      if ratio >= minimum:
        print(f'I: [openalex] search title found "{title}", similarity "{ratio}"')
        return i
      for fn1, fn2 in functions:
        if fn1(util.normalize(i['title'])) == fn2(util.normalize(title)):
          print(f'I: [openalex] search title found "{title}"')
          return i
  if max_ratio > 0:
    print(f'W: [openalex] search title not found "{title}", similarity "{max_ratio}"')
  else:
    print(f'W: [openalex] search title not found "{title}"')
  return False

def references(doi):
  doi_url = doi
  if not re.match("^http", doi_url, re.I):
    doi_url = "https://doi.org/" + doi
  try:
    work = Works()[doi_url]
    if work:
      print(f'I: [openalex] references for doi "{doi}"')
      references = work["referenced_works"]
      data = []
      for r in references:
        reference = Works()[r]
        if reference:
          data.append(reference)
        time.sleep(0.1)
      return data
    print(f'W: [openalex] doi not found "{doi}"')
  except Exception as e:
    print(f"E: [openalex] {e}")
  return False

def find_doi(data):
  if data and isinstance(data, dict) and 'doi' in data and data['doi']:
    doi = re.sub(r'^/+|^\s*https?://(dx.)?doi.org/*', '', data['doi'])
    return doi.lower()
  return False

def find_pmid(data):
  if data and isinstance(data, dict) and 'ids' in data and data['ids']:
    if 'pmid' in data['ids']:
      pmid = re.sub(r'^\s*https?://pubmed.ncbi.nlm.nih.gov/*|^\s*MEDLINE:', '', data['ids']['pmid'])
      return pmid
  return False

def feed(row, data):
  fed = False
  if 'publication_year' not in row and 'publication_year' in data:
    row['publication_year'] = data['publication_year']
    fed = True
  if 'document_title' not in row and 'title' in data:
    row['document_title'] = data['title']
    fed = True
  if 'author' in data:
    if 'id' not in row and 'id' in data['author']:
      row['id'] = data['author']['id']
      fed = True
    if 'orcid' not in row and 'orcid' in data['author']:
      row['orcid'] = data['author']['orcid']
      fed = True
    if 'author_name' not in row and 'display_name' in data['author']:
      row['author_name'] = data['author']['display_name']
      fed = True
  if 'authorships' in data:
    if 'document_authors' not in row:
      row['document_authors'] = "; ".join(map(lambda x: x['author']['display_name'], data['authorships']))
      fed = True
  if 'ids' in data:
    if 'pmid' not in row and 'pmid' in data['ids']:
      row['pmid'] = data['ids']['pmid']
      fed = True
    if 'pmcid' not in row and 'pmcid' in data['ids']:
      row['pmcid'] = data['ids']['pmcid']
      fed = True
  if 'isbn' not in row and 'isbn' in data:
    row['isbn'] = data['isbn']
    fed = True
  if 'issn' not in row and 'issn' in data:
    row['issn'] = data['issn']
    fed = True
  if 'language' not in row and 'language' in data:
    row['language'] = data['language']
    fed = True
  if fed:
    # add __name__ to api_source column
    name = __name__.split('.')[-1]
    if 'api_source' not in row:
      row['api_source'] = name
    else:
      if row['api_source'].find(name) < 0:
        row['api_source'] = f"{row['api_source']}+{name}"

def authors(doi):
  doi_url = doi
  if not re.match("^http", doi_url, re.I):
    doi_url = "https://doi.org/" + doi
  try:
    work = Works()[doi_url]
    if work and 'authorships' in work:
      print(f'I: [openalex] authors for doi "{doi}"')
      return work['authorships']
    print(f'W: [openalex] doi not found "{doi}"')
  except Exception as e:
    print(f"E: [openalex] {e}")
  return False

def query(searchstring):
  pages = Works().search_filter(fulltext=searchstring).paginate(per_page=200)
  data = []
  for page in pages:
    data = data + page
  if data:
    print(f'I: [openalex] query "{searchstring}" = {len(data)}')
    return data
  print(f'W: [openalex] query not found "{searchstring}"')
  return False

# id format: "W2041693718"
def find_id(data):
  if data and 'id' in data and data['id']:
    openalex_id = re.sub(r'https://openalex.org/', '', data['id'])
    return openalex_id
  return False

def filename(data):
  openalex_id = find_id(data)
  if openalex_id:
    return f"openalex-{openalex_id}.json".lower()
  return False
