import httpx
import re

# Web of Science (WOS) API documentation
# https://developer.clarivate.com/apis/wos

# WoS OpenAPI documentation
# https://api.clarivate.com/swagger-ui/?url=https%3A%2F%2Fdeveloper.clarivate.com%2Fapis%2Fwos%2Fswagger

# WoS API user Query fields doc
# https://images.webofknowledge.com/images/help/WOS/hs_advanced_fieldtags.html

# WoS WOS ID Accession Number documentation:
# https://support.clarivate.com/ScientificandAcademicResearch/s/article/Web-of-Science-Core-Collection-Searching-Accession-Numbers

# User Query Fields:
# AB=Abstract
# AD=Address
# ALL=All Fields
# AI=Author Identifiers
# AK=Author Keywords
# AU=Author
# CF=Conference
# CI=City
# CU=Country/Region
# DO=DOI
# ED=Editor
# FG=Grant Number
# FO=Funding Agency
# FT=Funding Text
# GP=Group Author
# IS=ISSN/ISBN
# KP=Keywords Plus®
# OG=Organization - Enhanced
# OO=Organization
# PMID=PubMed ID
# PS=Province/State
# PY=Year Published
# SA=Street Address
# SG=Suborganization
# SO=Publication Name
# SU=Research Area
# TI=Title
# TS=Topic
# UT=Accession Number
# WC=Web of Science Category
# ZP=Zip/Postal Code

apiKey = ""

name = __name__.split('.')[-1]

def fetch(doi):
  params = {
    'databaseId': 'WOK', # WOK represents all databases
    'count': 1, # Number of records to return, must be 0-100
    'usrQuery': f'DO="{doi}"', # User query for requesting data
    'sortField': 'RS+D', # Sort by Relevance Descending
  }
  headers = {
    'X-ApiKey': apiKey,
  }
  try:
    res = httpx.get("https://wos-api.clarivate.com/api/wos", params=params, headers=headers)
    json = res.json()
    if res.status_code == httpx.codes.OK:
      # json = {'Data': {'Records': {'records' ...} ...} ...}
      if json:
        if 'Data' in json and 'Records' in json['Data'] and 'records' in json['Data']['Records']:
          if 'REC' in json['Data']['Records']['records']:
            print(f'I: [wos] fetch doi "{doi}"')
            return json['Data']['Records']['records']['REC'][0]
        elif 'UID' in json or 'static_data' in json or 'dynamic_data' in json:
          print(f'I: [wos] fetch doi "{doi}"')
          return json
      print(f'W: [wos] empty response for doi "{doi}"')
    else:
      print(f"E: [wos] {json['message']}")
  except Exception as e:
    print(f'E: [wos] {e}')
  return False

def fetch_by_pmid(pmid):
  pmid = re.sub(r'^\s*https?://pubmed.ncbi.nlm.nih.gov/*|^\s*MEDLINE:', '', pmid)
  params = {
    'databaseId': 'WOK', # WOK represents all databases
    'count': 1, # Number of records to return, must be 0-100
    'usrQuery': f'PMID="{pmid}"', # User query for requesting data
    'sortField': 'RS+D', # Sort by Relevance Descending
  }
  headers = {
    'X-ApiKey': apiKey,
  }
  try:
    res = httpx.get("https://wos-api.clarivate.com/api/wos", params=params, headers=headers)
    json = res.json()
    if res.status_code == httpx.codes.OK:
      # json = {'Data': {'Records': {'records' ...} ...} ...}
      if json:
        if 'Data' in json and 'Records' in json['Data'] and 'records' in json['Data']['Records']:
          if 'REC' in json['Data']['Records']['records']:
            print(f'I: [wos] fetch pmid "{pmid}"')
            return json['Data']['Records']['records']['REC'][0]
        elif 'UID' in json or 'static_data' in json or 'dynamic_data' in json:
          print(f'I: [wos] fetch pmid "{pmid}"')
          return json
      print(f'W: [wos] empty response for pmid "{pmid}"')
    else:
      print(f"E: [wos] {json['message']}")
  except Exception as e:
    print(f'E: [wos] {e}')
  return False

def find(wos_id):
  params = {
    'databaseId': 'WOK', # WOK represents all databases
  }
  headers = {
    'X-ApiKey': apiKey,
  }
  try:
    res = httpx.get(f"https://wos-api.clarivate.com/api/wos/id/{wos_id}", params=params, headers=headers)
    json = res.json()
    if res.status_code == httpx.codes.OK:
      # json = {'Data': {'Records': {'records' ...} ...} ...}
      if json and 'Data' in json and 'Records' in json['Data'] and 'records' in json['Data']['Records']:
        if 'REC' in json['Data']['Records']['records']:
          print(f'I: [wos] find wosid "{wos_id}"')
          return json['Data']['Records']['records']['REC'][0]
    else:
      print(f"E: [wos] {json['message']}")
  except Exception as e:
    print(f'E: [wos] {e}')
  return False

def query(wos_id):
  params = {
    'databaseId': 'WOK', # WOK represents all databases
    'count': 1, # Number of records to return, must be 0-100
    'usrQuery': f'UT="{wos_id}"', # User query for requesting data
    'sortField': 'RS+D', # Sort by Relevance Descending
  }
  headers = {
    'X-ApiKey': apiKey,
  }
  try:
    res = httpx.get("https://wos-api.clarivate.com/api/wos", params=params, headers=headers)
    json = res.json()
    if res.status_code == httpx.codes.OK:
      # json = {'Data': {'Records': {'records' ...} ...} ...}
      if json:
        if 'Data' in json and 'Records' in json['Data'] and 'records' in json['Data']['Records']:
          if 'REC' in json['Data']['Records']['records']:
            print(f'I: [wos] query wosid "{wos_id}"')
            return json['Data']['Records']['records']['REC'][0]
        elif 'UID' in json or 'static_data' in json or 'dynamic_data' in json:
          print(f'I: [wos] query wosid "{wos_id}"')
          return json
      print(f'W: [wos] empty response for wosid "{wos_id}"')
    else:
      print(f"E: [wos] {json['message']}")
  except Exception as e:
    print(f'E: [wos] {e}')
  return False

def get(wos_id):
  return find(wos_id) or query(wos_id)

# match() methods combines search() and fetch() to retrieve metadata searching by title
def match(title):
  title_normalized = title.replace(" ", "").replace("-", "").lower()
  params = {
    'databaseId': 'WOK', # WOK represents all databases
    'count': 100, # Number of records to return, must be 0-100
    'usrQuery': f'TI="{title}"', # User query for requesting data
    'sortField': 'RS+D', # Sort by Relevance Descending
  }
  headers = {
    'X-ApiKey': apiKey,
  }
  try:
    res = httpx.get("https://wos-api.clarivate.com/api/wos", params=params, headers=headers)
    json = res.json()
    if res.status_code == httpx.codes.OK:
      # json = {'Data': {'Records': {'records' ...} ...} ...}
      if json and 'Data' in json and 'Records' in json['Data'] and 'records' in json['Data']['Records']:
        if 'REC' in json['Data']['Records']['records']:
          for i in json['Data']['Records']['records']['REC']:
            for t in i['static_data']['summary']['titles']['title']:
              if 'type' in t and t['type'] == 'item':
                if isinstance(t['content'], list):
                  t['content'] = t['content'][0]
                if t['content'].replace(" ", "").replace("-", "").lower() == title_normalized:
                  print(f'I: [wos] fetch title "{title}"')
                  return i
                if t['content'].replace(" ", "").replace("-", "").replace(",", "").replace(";", "").replace(".", "").lower() == title_normalized.replace(",", "").replace(";", "").replace(".", ""):
                  print(f'I: [wos] fetch title "{title}"')
                  return i
                if t['content'].replace(" ", "").replace("-", "").replace("[", "").replace("]", "").replace(".", "").lower() == title_normalized.replace("[", "").replace("]", "").replace(".", ""):
                  print(f'I: [wos] fetch title "{title}"')
                  return i
    else:
      print(f"E: [wos] {json['message']}")
  except Exception as e:
    print(f"E: [wos] {e}")
  return False

def find_doi(data):
  if data and 'dynamic_data' in data and 'cluster_related' in data['dynamic_data']:
    if 'identifiers' in data['dynamic_data']['cluster_related']:
      if isinstance(data['dynamic_data']['cluster_related']['identifiers']['identifier'], list):
        for i in data['dynamic_data']['cluster_related']['identifiers']['identifier']:
          if 'type' in i and (i['type'] == 'xref_doi' or i['type'] == 'doi'):
            return i['value'].lower()
      else:
        i = data['dynamic_data']['cluster_related']['identifiers']['identifier']
        if 'type' in i and (i['type'] == 'xref_doi' or i['type'] == 'doi'):
          return i['value'].lower()
  return False

def feed(row, data):
  if not data:
    return False
  fed = False
  if 'wos_uid' not in row and 'UID' in data:
    if re.match(r"^WOS:", data['UID']):
      row['wos_uid'] = data['UID']
      fed = True
  # author columns
  if 'static_data' in data:
    if 'fullrecord_metadata' in data['static_data']:
      if 'addresses' in data['static_data']['fullrecord_metadata']:
        if 'author_name' not in row and 'address_name' in data['static_data']['fullrecord_metadata']['addresses']:
          for k in ['name', 'organization', 'country', 'city', 'street', 'full_address']:
            row[f"author_{k}"] = []
          if isinstance(data['static_data']['fullrecord_metadata']['addresses']['address_name'], list):
            for address_name in data['static_data']['fullrecord_metadata']['addresses']['address_name']:
              if isinstance(address_name, dict) and 'address_spec' in address_name and 'names' in address_name:
                if isinstance(address_name['names']['name'], list):
                  names = address_name['names']['name']
                else:
                  names = [address_name['names']['name']]
                for name in names:
                  if 'wos_standard' in name:
                    row['author_name'].append(name['wos_standard'])
                    fed = True
                  else:
                    row['author_name'].append(name['display_name'])
                    fed = True
                  for k in ['country', 'city', 'street', 'full_address']:
                    row[f"author_{k}"] = row[f"author_{k}"] if f"author_{k}" in row else []
                    if k in address_name['address_spec']:
                      row[f"author_{k}"].append(address_name['address_spec'][k])
                      fed = True
                    else:
                      row[f"author_{k}"].append('NotFound')
                  if 'organizations' in address_name['address_spec'] and 'organization' in address_name['address_spec']['organizations']:
                    found_organization = False
                    for organization in address_name['address_spec']['organizations']['organization']:
                      if 'pref' in organization and organization['pref'] == 'Y':
                        row['author_organization'].append(organization['content'])
                        fed = True
                        found_organization = True
                        break
                    if not found_organization:
                      for organization in address_name['address_spec']['organizations']['organization']:
                        if 'content' in organization:
                          row['author_organization'].append(organization['content'])
                          fed = True
                          found_organization = True
                          break
                    if not found_organization:
                      row['author_organization'].append('NotFound')
  # other non-author columns
  if 'static_data' in data:
    if 'summary' in data['static_data']:
      if 'titles' in data['static_data']['summary']:
        for i in data['static_data']['summary']['titles']['title']:
          if 'source_title' not in row and i['type'] == 'source':
            row['source_title'] = i['content']
            fed = True
          if 'document_title' not in row and i['type'] == 'item':
            row['document_title'] = i['content']
            fed = True
      if 'doctype' not in row and 'doctypes' in data['static_data']['summary']:
        if 'doctype' in data['static_data']['summary']['doctypes']:
          if isinstance(data['static_data']['summary']['doctypes']['doctype'], list):
            row['doctype'] = "; ".join(data['static_data']['summary']['doctypes']['doctype'])
            fed = True
          else:
            row['doctype'] = data['static_data']['summary']['doctypes']['doctype']
            fed = True
      if 'pub_info' in data['static_data']['summary']:
        if 'publication_year' not in row and 'pubyear' in data['static_data']['summary']['pub_info']:
          row['publication_year'] = data['static_data']['summary']['pub_info']['pubyear']
          fed = True
      if 'document_authors' not in row and 'names' in data['static_data']['summary']:
        if isinstance(data['static_data']['summary']['names']['name'], list):
          row['document_authors'] = "; ".join(map(lambda x: x['wos_standard'] if 'wos_standard' in x else x['display_name'], data['static_data']['summary']['names']['name']))
        else:
          if 'wos_standard' in data['static_data']['summary']['names']['name']:
            row['document_authors'] = data['static_data']['summary']['names']['name']['wos_standard']
          else:
            row['document_authors'] = data['static_data']['summary']['names']['name']['display_name']
        fed = True
      if 'EWUID' in data['static_data']['summary']:
        if 'WUID' in data['static_data']['summary']['EWUID']:
          if 'coll_id' in data['static_data']['summary']['EWUID']['WUID']:
            row['coll_id'] = data['static_data']['summary']['EWUID']['WUID']['coll_id']
            fed = True
    if 'fullrecord_metadata' in data['static_data']:
      if 'languages' in data['static_data']['fullrecord_metadata']:
        if 'language' not in row and 'language' in data['static_data']['fullrecord_metadata']['languages']:
          row['language'] = data['static_data']['fullrecord_metadata']['languages']['language']['content']
          fed = True
      if 'keywords' in data['static_data']['fullrecord_metadata']:
        if 'keywords' not in row and 'keyword' in data['static_data']['fullrecord_metadata']['keywords']:
          if isinstance(data['static_data']['fullrecord_metadata']['keywords']['keyword'], list):
            row['keywords'] = "; ".join(map(lambda x: x['content'] if isinstance(x, dict) else str(x), data['static_data']['fullrecord_metadata']['keywords']['keyword']))
            fed = True
          else:
            row['keywords'] = data['static_data']['fullrecord_metadata']['keywords']['keyword']
            fed = True
      if 'addresses' in data['static_data']['fullrecord_metadata']:
        organizations_not_in_row = 'organizations' not in row
        if organizations_not_in_row and 'address_name' in data['static_data']['fullrecord_metadata']['addresses']:
          if isinstance(data['static_data']['fullrecord_metadata']['addresses']['address_name'], list):
            for x in data['static_data']['fullrecord_metadata']['addresses']['address_name']:
              if isinstance(x, dict) and 'address_spec' in x:
                for k in ['country', 'city', 'street', 'full_address']:
                  if k in x['address_spec']:
                    row[k] = x['address_spec'][k]
                    fed = True
                if organizations_not_in_row and 'organizations' in x['address_spec'] and 'organization' in x['address_spec']['organizations']:
                  for o in x['address_spec']['organizations']['organization']:
                    if 'pref' in o and o['pref'] == 'Y':
                      if 'organizations' not in row:
                        row['organizations'] = o['content']
                      else:
                        row['organizations'] = row['organizations'] + "; " + o['content']
                      fed = True
          else:
            x = data['static_data']['fullrecord_metadata']['addresses']['address_name']
            for k in ['country', 'city', 'street', 'full_address']:
              if k in x['address_spec']:
                row[k] = x['address_spec'][k]
                fed = True
      if 'category_info' in data['static_data']['fullrecord_metadata']:
        if 'wos_category' not in row and 'subjects' in data['static_data']['fullrecord_metadata']['category_info']:
          row['wos_category'] = "; ".join(map(lambda x: x['content'], data['static_data']['fullrecord_metadata']['category_info']['subjects']['subject']))
          fed = True
        if 'research_area' not in row and 'headings' in data['static_data']['fullrecord_metadata']['category_info']:
          if isinstance(data['static_data']['fullrecord_metadata']['category_info']['headings']['heading'], list):
            row['research_area'] = "; ".join(data['static_data']['fullrecord_metadata']['category_info']['headings']['heading'])
            fed = True
          else:
            row['research_area'] = data['static_data']['fullrecord_metadata']['category_info']['headings']['heading']
            fed = True
        if 'subject_extended' not in row:
          if 'subjects' in data['static_data']['fullrecord_metadata']['category_info']:
            subjects = data['static_data']['fullrecord_metadata']['category_info']['subjects']['subject']
            subjects = filter(lambda x: x['ascatype'] == 'extended', subjects)
            row['subject_extended'] = "; ".join(map(lambda x: x['content'], subjects))
            fed = True
        if 'subject_traditional' not in row:
          if 'subjects' in data['static_data']['fullrecord_metadata']['category_info']:
            subjects = data['static_data']['fullrecord_metadata']['category_info']['subjects']['subject']
            subjects = filter(lambda x: x['ascatype'] == 'traditional', subjects)
            row['subject_traditional'] = "; ".join(map(lambda x: x['content'], subjects))
            fed = True
      if 'abstracts' in data['static_data']['fullrecord_metadata']:
        if 'abstract' not in row and 'abstract' in data['static_data']['fullrecord_metadata']['abstracts']:
          if 'abstract_text' in data['static_data']['fullrecord_metadata']['abstracts']['abstract']:
            if isinstance(data['static_data']['fullrecord_metadata']['abstracts']['abstract']['abstract_text']['p'], list):
              row['abstract'] = data['static_data']['fullrecord_metadata']['abstracts']['abstract']['abstract_text']['p'][0]
            else:
              row['abstract'] = data['static_data']['fullrecord_metadata']['abstracts']['abstract']['abstract_text']['p']
            fed = True
    if 'item' in data['static_data']:
      if 'keywords_plus' in data['static_data']['item']:
        if 'keywords_plus' not in row and 'keyword' in data['static_data']['item']['keywords_plus']:
          if isinstance(data['static_data']['item']['keywords_plus']['keyword'], list):
            row['keywords_plus'] = "; ".join(map(str, data['static_data']['item']['keywords_plus']['keyword']))
            fed = True
          else:
            row['keywords_plus'] = data['static_data']['item']['keywords_plus']['keyword']
            fed = True
  if 'dynamic_data' in data:
    if 'cluster_related' in data['dynamic_data']:
      if 'identifiers' in data['dynamic_data']['cluster_related']:
        if isinstance(data['dynamic_data']['cluster_related']['identifiers']['identifier'], list):
          for i in data['dynamic_data']['cluster_related']['identifiers']['identifier']:
            if 'doi' not in row and 'type' in i and i['type'] == 'xref_doi':
              row['doi'] = i['value']
              fed = True
            elif i['type'] not in row:
              row[i['type']] = i['value']
              fed = True
        else:
          i = data['dynamic_data']['cluster_related']['identifiers']['identifier']
          if 'doi' not in row and 'type' in i and i['type'] == 'xref_doi':
            row['doi'] = i['value']
            fed = True
          elif i['type'] not in row:
            row[i['type']] = i['value']
            fed = True
    if 'wos_usage' in data['dynamic_data'] and 'wos_usage' not in row:
      row['wos_usage'] = data['dynamic_data']['wos_usage']['alltime']
      fed = True
  if fed:
    # add __name__ to api_source column
    name = __name__.split('.')[-1]
    if 'api_source' not in row:
      row['api_source'] = name
    else:
      if row['api_source'].find(name) < 0:
        row['api_source'] = f"{row['api_source']}+{name}"
  return False

def find_coll_id(data):
  if data and 'static_data' in data and 'summary' in data['static_data']:
    if 'EWUID' in data['static_data']['summary']:
      if 'WUID' in data['static_data']['summary']['EWUID']:
        if 'coll_id' in data['static_data']['summary']['EWUID']['WUID']:
          return data['static_data']['summary']['EWUID']['WUID']['coll_id']
  return False

def find_uid(data):
  if data and 'UID' in data:
    return data['UID']
  return False

def filename(data):
  coll_id = find_coll_id(data)
  uid = find_uid(data)
  if coll_id and uid:
    uid = re.sub(r'.+:', '', uid)
    return f"{coll_id}-{uid}.json".lower()
  return False
