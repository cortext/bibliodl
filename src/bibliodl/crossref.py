from crossref.restful import Works, Etiquette
from habanero import Crossref
import re
from bibliodl.util import string as util

# Crossref API documentation
# https://github.com/CrossRef/rest-api-doc/blob/master/api_format.md

# Crossref API swagger documentation
# https://api.crossref.org/swagger-ui/index.html

# CrossRef API JSON example
# https://api.crossref.org/works/10.1155/2014/413629

# Crossref API example search doi by title
# https://api.crossref.org/works?sort=relevance&rows=1&query.title=Controversy%20Mapping%20and%20the%20Care%20for%20Climate%20Commons:%20Re-assembling%20the%20Danish%20Climate%20Movement%20by%20Counter-Mapping%20Digital%20Network%20Maps

et = Etiquette('bibliodl', '0.1.0', 'https://gitlab.com/cortext/cortext-libraries/bibliodl', '')
works = Works(etiquette=et)
cr = Crossref(ua_string='bibliodl/0.1.0', mailto='')

name = __name__.split('.')[-1]

def fetch(doi):
  try:
    data = works.doi(doi)
    if data:
      print(f'I: [crossref] fetch doi "{doi}"')
      return data
    else:
      print(f'W: [crossref] empty response for doi "{doi}"')
  except Exception as e:
    print(f"E: [crossref] {e}")
  return False

def search(title):
  try:
    data = cr.works(query_title=title)
    max_ratio = 0
    if data:
      functions = [
        [util.nop, util.nop],
        [util.subtitle, util.nop],
        [util.maintitle, util.nop],
        [util.nonquote, util.nonquote],
        [util.nonascii, util.nonascii],
        [util.nonpunctuation, util.nonpunctuation],
        [util.nonbracket, util.nonbracket]
      ]
      for i in data['message']['items']:
        for t in i['title']:
          ratio, minimum = util.ratio(t, title)
          if ratio > max_ratio:
            max_ratio = ratio
          if ratio >= minimum:
            print(f'I: [crossref] search title found "{title}", similarity "{ratio}"')
            return i
        for t in i['title']:
          for fn1, fn2 in functions:
            if fn1(util.normalize(t)) == fn2(util.normalize(title)):
              print(f'I: [crossref] search title found "{title}"')
              return i
    print(f'W: [crossref] search title not found "{title}", similarity "{max_ratio}"')
  except Exception as e:
    print(f"E: [crossref] {e}")
  return False

def find_doi(data):
  if data and 'DOI' in data:
    doi = re.sub(r'^/+|^\s*https?://(dx.)?doi.org/*', '', data['DOI'])
    return doi.lower()
  return False

def find_pmid(data):
  # TODO
  return False

def feed(row, data):
  fed = False
  if 'crossref_citations' not in row and 'is-referenced-by-count' in data:
    # [crossref api] is-referenced-by-count: Count of inbound references deposited with Crossref
    row['crossref_citations'] = data['is-referenced-by-count']
    fed = True
  if 'crossref_references' not in row and 'references-count' in data:
    # [crossref api] references-count: Count of outbound references deposited with Crossref
    row['crossref_references'] = data['references-count']
    fed = True
  if 'publication_year' not in row and 'publication_year' in data:
    row['publication_year'] = data['publication_year']
    fed = True
  if 'document_title' not in row and 'title' in data:
    if type(data['title']) is list:
      if len(data['title']) > 0:
        row['document_title'] = data['title'][0]
        fed = True
    else:
      row['document_title'] = data['title']
      fed = True
  if 'publication_year' not in row and 'year' in data:
    row['publication_year'] = data['year']
    fed = True
  # crossref JSON excerpt: "... 'published': {'date-parts': [[2016, 10, 15]]} ..."
  if 'publication_year' not in row and 'published' in data:
    if 'date-parts' in data['published']:
      if len(data['published']['date-parts']) > 0:
        row['publication_year'] = data['published']['date-parts'][0][0]
        fed = True
  if 'ORCID' in data:
    row['orcid'] = data['ORCID']
    fed = True
  if 'given' in data and 'family' in data:
    row['author_name'] = f"{data['given']} {data['family']}"
    fed = True
  if fed:
    # add __name__ to api_source column
    name = __name__.split('.')[-1]
    if 'api_source' not in row:
      row['api_source'] = name
    else:
      if row['api_source'].find(name) < 0:
        row['api_source'] = f"{row['api_source']}+{name}"

def authors(doi):
  data = works.doi(doi)
  if data and 'author' in data and len(data['author']) > 0:
    print(f'I: [crossref] authors for doi "{doi}"')
    return data['author']
  return False

def references(doi):
  data = works.doi(doi)
  if data and 'reference' in data:
    print(f'I: [crossref] references for doi "{doi}"')
    return data['reference']
  return False

def filename(data):
  doi = find_doi(data)
  if doi:
    doi = doi.replace('/', '-').replace('(', '_').replace(')', '_')
    return f"doi-{doi}.json".lower()
  return False
