from collections.abc import Iterable
from pybliometrics.scopus import PlumXMetrics
from pybliometrics.scopus.exception import Scopus404Error

# PlumX Metrics documentation
# https://plumanalytics.com/learn/about-metrics/

# PlumX Metrics API documentation
# https://dev.elsevier.com/documentation/PlumXMetricsAPI.wadl

# Python PlumXMetrics documentation
# https://pybliometrics.readthedocs.io/en/stable/classes/PlumXMetrics.html

# Examples
# ?doi=10.1590%2Fs0102-695x2007000100021
# https://api.plu.mx/a/?doi=10.1590%2Fs0102-695x2007000100021
# https://api.plu.mx/r/doi=10.1590%2Fs0102-695x2007000100021

def fetch(doi):
  try:
    data = PlumXMetrics(doi, id_type='doi')
    if data.category_totals:
      print(f'I: [plumx] fetch doi "{doi}"')
      return data
    else:
      print(f'W: [plumx] empty response for doi "{doi}"')
      return False
  except Scopus404Error as err:
    print(f'E: [plumx] Scopus not found for doi "{doi}" - {err=}')
    return False
  except Exception as err:
    print(f'E: [plumx] unknown error for doi "{doi}" - {err=}')
    return False

def feed(row, data):
  ## category_totals
  t = data.category_totals
  if 'plumx_score' not in row:
    row['plumx_score'] = t[0].total
  citation_indexes = next((i.total for i in t if i.name == 'citation'), None)
  if citation_indexes:
    row['plumx_citation_indexes'] = citation_indexes
  ## social_media
  t = data.social_media
  if isinstance(t, Iterable):
    row['plumx_social_media'] = sum([x.total for x in t])
  else:
    row['plumx_social_media'] = t
  if t:
    facebook = next((i.total for i in t if i.name == 'FACEBOOK_COUNT'), None)
    if facebook:
      row['plumx_facebook'] = facebook
  ## citation
  t = data.citation
  if t:
    policy_citation = next((i.total for i in t if i.name == 'Policy Citation'), None)
    if policy_citation:
      row['plumx_policy_citations'] = policy_citation
    patent_family_citation = next((i.total for i in t if i.name == 'Patent Families'), None)
    if patent_family_citation:
      row['plumx_patent_family_citations'] = patent_family_citation
  # add __name__ to api_source column
  name = __name__.split('.')[-1]
  if 'api_source' not in row:
    row['api_source'] = name
  else:
    if row['api_source'].find(name) < 0:
      row['api_source'] = f"{row['api_source']}+{name}"
