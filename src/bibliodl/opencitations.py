import opencitingpy

# OpenCitation API documentation
# https://opencitations.net/index/api/v2
# https://opencitations.net/meta/api/v2

# OpenCitation API JSON examples
# https://opencitations.net/index/api/v2/citations/doi:10.1108/jd-12-2013-0166
# https://opencitations.net/index/ci/06204358443-062501777134.json

# opencitingpy library source code repository
# https://pypi.org/project/opencitingpy

client = opencitingpy.client.Client()

def references(doi):
  data = client.get_references(doi)
  if data:
    print(f'I: [opencitations] references for doi "{doi}"')
    return data
  print(f'W: [opencitations] doi not found "{doi}"')
  return False

def find_doi(data):
  if data:
    return data.cited
  return False

def feed(row, data):
  # add __name__ to api_source column
  name = __name__.split('.')[-1]
  if 'api_source' not in row:
    row['api_source'] = name
  else:
    if row['api_source'].find(name) < 0:
      row['api_source'] = f"{row['api_source']}+{name}"
